package cpp.spike.bulkscan;

import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

public class Function {
    /**
     * This function listens at endpoint "/api/hello". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/hello
     * 2. curl {your host}/api/hello?name=HTTP%20Query
     */
    @FunctionName("scanReceived")
    public void logEvent(
            @EventGridTrigger(name = "event")
                    EventSchema event,
            final ExecutionContext context) {
        // log
        context.getLogger().info("Event content: ");
        context.getLogger().info("Subject: " + event.subject);
        context.getLogger().info("Time: " + event.eventTime); // automatically converted to Date by the runtime
        context.getLogger().info("Id: " + event.id);
        context.getLogger().info("Data: " + event.data);
        context.getLogger().info("===============================");
        context.getLogger().info("file url: " + event.data.get("url"));
        context.getLogger().info("CPUUID for onward call to PCF: " + System.getenv("CPUUID"));

    }
}

